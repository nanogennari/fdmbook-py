# Python code from the Finite Difference Methods for Ordinary and Partial Differential Equations book's supplemetary material

**WARNING:** This is a work in progress.

Functions from Randall J. LeVeque's book Finite Difference Methods for Ordinary and Partial Differential Equations [supplementary material](https://staff.washington.edu/rjl/fdmbook/) ported to Python.

## To be implemented:

* advection_LW_pbc.m
* bvp_2.m
* bvp_4.m
* bvp_spectral.m
* chap1example1.m
* chap2fig2.m
* chebpoly.m
* chebpoly1.m
* chebpoly2.m
* decay1.m
* decaytest.m
* error_loglog.m
* error_table.m
* ex7p11.m
* fdcoeffF.m
* ~~fdcoeffV.m~~
* fdstencil.m
* heat_CN.m
* iter_bvp_Asplit.m
* makeplotBL.m
* makeplotS.m
* odesample.m
* odesampletest.m
* plotBL.m
* plotS.m
* plotSrkc.m
* poisson.m
* springmass.m
* xgrid.m

## Documentation

Documetation can be found [here](https://nanogennari.gitlab.io/fdmbook-py/).