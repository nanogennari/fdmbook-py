import numpy as np
from scipy.special import factorial


def fdcoeffV(k: int, xbar: np.ndarray, x: np.ndarray) -> np.ndarray:
    """Compute coefficients for finite difference approximation for the
        derivative of order k at xbar based on grid values at points in x.

    !!! warning
        This approach is numerically unstable for large values of n since
            the Vandermonde matrix is poorly conditioned.  Use fdcoeffF.m instead,
            which is based on Fornberg's method.

    This function returns a row vector c of dimension 1 by n, where n=length(x),
        containing coefficients to approximate u^{(k)}(xbar),
        the k'th derivative of u evaluated at xbar,  based on n values
        of u at x(1), x(2), ... x(n).

    If U is a column vector containing u(x) at these n points, then
        c*U will give the approximation to u^{(k)}(xbar).

    !!! note
        For k=0 this can be used to evaluate the interpolating polynomial
            itself.

    Requires length(x) > k.
    Usually the elements x(i) are monotonically increasing
        and x(1) <= xbar <= x(n), but neither condition is required.
    The x values need not be equally spaced but must be distinct.

    From  http://www.amath.washington.edu/~rjl/fdmbook/  (2007)

    Args:
        k (int): Order of the derivative.
        xbar (np.ndarray): Point to expand arround.
        x (np.ndarray): Grid values to be used.

    Returns:
        np.ndarray: Calculated coefficients.
    """

    # Converts x to a numpy array
    x = np.array(x)
    n = np.prod(x.shape)
    # Check if there is enough points in x
    if k >= n:
        raise ValueError("len(x) must be larger than k")

    # Forces x to be a line vector
    x = np.reshape(x, n)

    # Initialize A
    A = np.ones((n, n))

    # Calculate A values row by row
    xrow = x - xbar
    for i in range(1, n):
        A[i, :] = np.power(xrow, (i)) / factorial(i)

    # Initialize b
    b = np.zeros(n)
    b[k] = 1

    # Solve linear system and coefficients it
    return np.linalg.solve(A, b)
