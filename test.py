import fdmbook
import unittest
import numpy as np


class test_fdcoeffV(unittest.TestCase):
    def test_1(self):
        np.testing.assert_array_almost_equal(
            fdmbook.fdcoeffV(1, 1, [1, 1.05]), [-20, 20]
        )

    def test_2(self):
        np.testing.assert_array_almost_equal(
            fdmbook.fdcoeffV(1, 1, [0.95, 1, 1.05]), [-10, 0, 10]
        )

    def test_3(self):
        np.testing.assert_array_almost_equal(
            fdmbook.fdcoeffV(2, 1, [0.95, 1, 1.05]), [400, -800, 400]
        )


if __name__ == "__main__":
    unittest.main()
